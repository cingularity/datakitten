import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    MAIL_SSERVER = 'mail1.pwhost.de'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or 'amit@ashah.de'
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or 'Dragon1234'
    MYPROJ_NAME = "Datenium"
    MYPROJ_MAIL_SUBJECT_PREFIX = '[Datenium]'
    MYPROJ_MAIL_SENDER = 'Datenium Admin <amit@ashah.de>'
    MYPROJ_ADMIN = 'amit@ashah.de'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    BABEL_DEFAULT_LOCALE = 'en'
    LANGUAGES = {
        'de-DE': 'Deutsch',
        'en-US': 'English'
    }
    
    
    
    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'dev.sqlite')
    
    
class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'test.sqlite')
    

class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('STAGE_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'stage.sqlite')
    
    
class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('MAIN_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    
    
config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
    
    'default': DevelopmentConfig
    
}
