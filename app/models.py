from datetime import datetime
import hashlib

from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin, AnonymousUserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app, request

from . import db, login_manager



class Permission(object):
    FOLLOW = 0x01
    WRITE_ARTICLES = 0x02
    WRITE_OFFERS = 0x04
    MODERATE_ARTICLES = 0x08
    MODERATE_OFFERS = 0x10
    ACCESS_BASIC = 0x20
    ACCESS_PREMIUM = 0x30
    ACCESS_PREMIUM_PLUS = 0x40
    ADMINISTER = 0x80



class Role(db.Model):
    __tablename__ = 'roles'
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    user = db.relationship('User', backref='role', lazy='dynamic')
    
    
    def __repr__(self):
        return "<Role %r>" % self.name
    
    
    @staticmethod
    def insert_roles():
        roles = {
            'User': (Permission.FOLLOW | 
                     Permission.WRITE_ARTICLES, True),
            'Basic' : (Permission.FOLLOW |
                       Permission.WRITE_OFFERS | 
                       Permission.ACCESS_BASIC, False),
            'Premium' : (Permission.FOLLOW |
                         Permission.WRITE_OFFERS | 
                         Permission.ACCESS_PREMIUM, False),
            'Premium-Plus' : (Permission.FOLLOW |
                              Permission.WRITE_OFFERS | 
                              Permission.ACCESS_PREMIUM_PLUS, False),
            'Moderator': (Permission.FOLLOW |
                          Permission.MODERATE_ARTICLES |
                          Permission.MODERATE_OFFERS, False),
            'Administrator': (0xff, False)
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role=Role(name=r)
            role.permissions = roles[r][0]
            role.default = roles[r][1]
            db.session.add(role)
        db.session.commit()
    
    

    
    
class User(UserMixin, db.Model):
    __tablename__ = 'users'
    
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    confirmed = db.Column(db.Boolean, default=False)
    name = db.Column(db.String(64))
    location = db.Column(db.String(64))
    about_me = db.Column(db.Text())
    article = db.relationship('Article', backref='author', lazy='dynamic')
    member_since = db.Column(db.DateTime(), default=datetime.utcnow)
    last_seen = db.Column(db.DateTime(), default=datetime.utcnow)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    
    
    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['MYPROJ_ADMIN']:
                self.role = Role.query.filter_by(permissions=0xff).first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()
    
    def __repr__(self):
        return '<User %r>' % self.username
    
    
    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')
    
    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)
        
    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})
    
    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True
    
    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})
    
    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        return True
    
    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})
    
    def change_email(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        return True
    
    def ping(self):
        self.last_seen = datetime.utcnow()
        db.session.add(self)
    
    def can(self, permissions):
        return self.role is not None \
            and (self.role.permissions & permissions) == permissions
            
    def is_administrator(self):
        return self.can(Permission.ADMINISTER)
    
    
class AnonymousUser(AnonymousUserMixin):
    def can(self, permissions):
        return False
    
    def is_administrator(self):
        return False
    

login_manager.anonymous_user = AnonymousUser        



class Article(db.Model):
    __tablename__ = 'articles'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.Text)
    timestamp = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    
    
    
class Project(db.Model):
    __tablename__ = 'projects'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    url = db.Column(db.String())
    requirements = db.Column(db.Text)
    pages_count = db.Column(db.Integer)
    configured = db.Column(db.Boolean(), default=False)
    deleted = db.Column(db.Boolean(), default=False)
    created_on = db.Column(db.DateTime(), index=True, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime(), index=True, default=datetime.utcnow, onupdate=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
   
    
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

