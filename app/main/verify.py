


def client(user):
    if user.role.name == 'User' or user.role.name == 'Moderator' \
        or user.role.name == 'Administrator':
        return False
    return True