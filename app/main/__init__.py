from flask import Blueprint, request


main = Blueprint('main', __name__)

from . import views, errors
from ..models import Permission
from app import babel

@main.app_context_processor
def inject_permissions():
    return dict(Permission=Permission)

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['de', 'en'])
