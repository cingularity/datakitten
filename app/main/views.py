from flask import render_template, redirect, url_for, abort, \
        flash, session, request
from flask_login import login_required, current_user
from flask_babel import gettext as _

from . import main
from .. import db, babel
from ..models import User, Permission, Role, Article
from .forms import EditProfileForm, EditProfileAdminForm, ArticleForm
from ..decorators import admin_required, permission_required
from .verify import client



@main.route("/")
def index():
    '''
    form = ArticleForm()
    if current_user.can(Permission.WRITE_ARTICLES) and \
        form.validate_on_submit():
        article = Article(body=form.body.data, 
            author=current_user._get_current_object())
        db.session.add(article)
        return redirect(url_for('.index'))
    articles = Article.query.order_by(Article.timestamp.desc()).all()
    '''
    return render_template('index.html')#, form=form, articles=articles)

'''
@main.route('/dashboard/<username>')
@login_required
def dashboard(username):
    user = User.query.filter_by(username=username).first()
    if (user is None):# or client(user):
        abort(404)
    return render_template('proj/dashboard.html', user=user)

'''

@main.route('/client/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first()
    if (user is None):# or client(user):
        abort(404)
    return render_template('user.html', user=user)


@main.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        current_user.name = form.name.data
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data
        db.session.add(current_user)
        flash(_('Your profile has been updated.'))
        return redirect(url_for('.user', username=current_user.username))
    form.name.data = current_user.name
    form.location.data = current_user.location
    form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', form=form)


@main.route("/edit-profile/<int:id>", methods=['GET', 'POST'])
@login_required
@admin_required
def edit_profile_admin(id):
    user = User.query.get_or_404(id)
    form = EditProfileAdminForm(user=user)
    if form.validate_on_submit():
        user.email = form.email.data
        user.username = form.username.data
        user.confirmed = form.confirmed.data
        user.role = Role.query.get(form.role.data)
        user.name = form.name.data
        user.location = form.location.data
        user.about_me = form.about_me.data
        db.session.add(user)
        flash(_('The profile has been updated.'))
        return redirect(url_for('.user', username=user.username))
    form.email.data = user.email
    form.username.data = user.username
    form.confirmed.data = user.confirmed
    form.role.data = user.role_id
    form.name.data = user.name
    form.location.data = user.location
    form.about_me.data = user.about_me
    return render_template('edit_profile.html', form=form, user=user)


@main.route("/article/<int:id>")
def article(id):
    article = Article.query.get_or_404(id)
    return render_template('article.html', articles=[article])


@main.route("/edit/<int:id>", methods=['GET', 'POST'])
@login_required
def edit(id):
    article = Article.query.get_or_404(id)
    if current_user != article.author and not current_user.can(Permission.ADMINISTER):
        abort(403)
    form = ArticleForm()
    if form.validate_on_submit():
        article.name = form.name.data
        article.start_on = form.start_on.data
        article.old_new = form.old_new.data
        article.body = form.body.data
        db.session.add(article)
        flash(_('The post has been updated.'))
        return redirect(url_for('article', id=article.id))
    form.name.data = article.name
    form.start_on.data = article.start_on
    form.old_new.data = article.old_new
    form.body.data = article.body
    return render_template('edit_article.html', form=form)
        
        