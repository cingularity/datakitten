from wtforms import StringField, SubmitField, BooleanField, SelectField, TextAreaField, ValidationError
from flask_babel import gettext, lazy_gettext
from flask_wtf import Form
from wtforms.validators import Required, Length, Email, Regexp

from ..models import Role, User

    
    
class EditProfileForm(Form):
    name = StringField(lazy_gettext('Real name'), validators=[Length(0, 64)])
    location = StringField(lazy_gettext('Location'), validators=[Length(0, 64)])
    about_me = TextAreaField(lazy_gettext('About me'))
    submit = SubmitField(lazy_gettext('Submit'))
    
    
class EditProfileAdminForm(Form):
    email = StringField(lazy_gettext('Email'), validators=[Required(), Length(1, 64), Email()])
    username = StringField(lazy_gettext('Username'), validators=[Required(), Length(1, 64),
            Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0, gettext('Usernames must have only letters, numbers, dots or underscores.'))
    ])
    confirmed = BooleanField(lazy_gettext('Confirmed'))
    role = SelectField(lazy_gettext('Role'), coerce=int)
    name = StringField(lazy_gettext('Real name'), validators=[Length(0, 64)])
    location = StringField(lazy_gettext('Location'), validators=[Length(0, 64)])
    about_me = TextAreaField(lazy_gettext('About_me'))
    submit = SubmitField(lazy_gettext('Submit'))
    
    
    def __init__(self, user, *args, **kwargs):
        super(EditProfileAdminForm, self).__init__(*args, **kwargs)
        self.role.choices = [(role.id, role.name)
                             for role in Role.query.order_by(Role.name).all()]
        self.user = user
        
    def validate_email(self, field):
        if field.data != self.user.email and User.query.filter_by(email=field.data).first():
            raise ValidationError(gettext('Email already registered.'))
        
    def validate_username(self, field):
        if field.data != self.user.username and User.query.filter_by(username=field.data).first():
            raise ValidationError(gettext('Username already in use.'))
        
        
class ArticleForm(Form):
    body = TextAreaField(lazy_gettext('What do I want'), validators=[Required()])
    submit = SubmitField(lazy_gettext('Submit'))