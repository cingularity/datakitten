��    3      �              L  3   M  6   �  
   �  R   �  G        ^  �   f     �          -  
   B     M     d     {     �     �     �     �     �     �  	             #  $   )     N     S     n     �     �  	   �     �     �      �  	   �  	   �          
          &  0   /     `     {     �  ?   �     �  (     (   5  $   ^     �     �  �  �  &   P
  &   w
     �
  A   �
  E   �
     2  �   :     �     �     �               2     M     e     }     �     �     �     �     �     �       $        -     2     R     f  $   z     �  	   �     �     �     �     �     �       	   !  	   +  =   5     s     �     �  `   �     /  "   H  )   k     �     �     �   A confirmation email has been sent to you by email. A new confirmation email hs been sent to you by email. Admin page An email with instructions to confirm your new email address has been sent to you. An email with instructions to reset your password has been sent to you. Article Before you can access this site you need to confirm your account.
        Check your inbox, you should have received an email with a confirmation link. Change Email Address Change Your Email Address Change Your Password Click here Click here to register Click here to reset it Confirm Your Account Confirm your account Confirm your email address Edit Article Edit Profile Email already registered. For admin only Forbidden Forgot your password? Hello Hello, {{ current_user.username }}!  Home Invalid email or password. Invalid password. Invalid request. Invalid username or password. Last seen Login Member since Need another confirmation email? New user? Not Found Register Reset Your Password Sign In Sign Out The confirmation link is invalid or has expired. The post has been updated. The profile has been updated. Username already in use. Usernames must have only letters, numbers, dots or underscores. You have been logged out. You have confirmed your account. Thanks! You have not confirmed your account yet. Your email address has been updated. Your password has been updated. Your profile has been updated. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2016-06-19 23:38+0200
PO-Revision-Date: 2016-06-19 23:47+0100
Last-Translator: Amit Shah <info@ashah.de>
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
 Eine bestätigungs Email ist verschikt Eine bestätigungs Email ist verschikt Admin Seite eine Email mit Anleitung zum Emailzurückstellen ist verschickt.  eine Email mit Anleitung zum Passwort zurückstellen ist verschickt.  Article Bevor du weiter diese Seite Nutzen kannst, du must dein Konto bestätigen 
Sehe deine Email, du musst eine Email von uns bekommen haben. Email Adresse ändern Dein Email adresse ändern Password ändern Hier Clicken Hier klicken um zu Registrieren Klicken um zurückzusetzen Bestätige deinen Konto Bestätige deine Email. Bestätige deine Email Artikle editieren Profil editieren Email schon registriert. Nur für Admin Nichtzulässig Passwort vergessen? Hallo Hello, {{ current_user.username }}!  Home Ungültige Email oder Passwort. Passwort ungültig. Ungültige Anfrage. ungültige Benutzername und Passwort Letze logout Einloggen Mitglied seit Neue Bestätigungs email? Neue Benutzer Nicht gefunden Registrieren Passwort zurückstellen. Einloggen Ausloggen Die bestätigung link ist entweder ungültig oder abgelaufen. Der Article ist aktualisiert. Die Profil ist aktualisiert. Benutzername ist schon vergeben Benutzername kann nur die Kombination der Buchstaben, Zahlen, punkte und untrstriche beinhalten. Du wurdest  Ausgelogged. Deine Konto ist bestätigt, Danke! Du hast dein Konto noch nicht bestätigt. Deine Email ist aktualisiert. Dein Passwort ist aktualisiert. Dein Profil ist aktualisiert. 