from flask import render_template, redirect, request, url_for, flash
from flask_login import login_user, login_required, logout_user, current_user
from flask_babel import gettext as _
import os

from . import proj
from ..models import User, Project
from .forms import NewProjectForm, ProjectButton
from .. import db
from ..email import send_email
from config import basedir


@proj.route("/")
def index():
    return render_template('proj/index.html')
    

'''    
@proj.route('proj/dashboard')
@login_required
def dashboard(username):
    user = User.query.filter_by(username=username).first()
    if (user is None):# or client(user):
        abort(404)
    return render_template('proj/dashboard.html', user=user)

'''

@proj.route("/dashboard", methods=['GET', 'POST'])
@login_required
def dashboard():
    form = NewProjectForm()
    projects = db.session.query(Project).filter_by(author_id=current_user.id).order_by(db.desc(Project.created_on)).all()
    if form.validate_on_submit():
        if projects is not None:
            '''Check only projects from the same user that are not deleted.'''
            project_name = [project_name.name for project_name in projects]
            deleted_project = []
            for project in projects:
                if project.deleted is False:
                    deleted_project.append(project.name)
            if form.name.data in project_name and deleted_project:
                flash(_('Please choose a new name. It already exists.')) 
            else:
                url = sanitize_url(form.url.data) # sanitize url to add http://
                name = form.name.data
                new_project=Project(name=name, url=url,
                    requirements=form.requirements.data, author_id=current_user.id)
                db.session.add(new_project)
                db.session.commit()
                latest_project = db.session.query(Project).filter_by(name=name).order_by(db.desc(Project.created_on)).first()
                scrap_template(name, url, latest_project.id) # create template for the project
                flash(_('Your new Project has been added. You shall receive an email as' \
                    ' soon as we are done with the configuration.'))
            return redirect(url_for('.dashboard'))
        
    return render_template('proj/dashboard.html', projects=projects, form=form)


def sanitize_url(url):
    if 'http://' or 'https://' in url[0:8]:
        url = url
    else:
        url = 'http://' + url
    return url

def scrap_template(name, url, id):
    dir_name = os.path.join(basedir, "app/ScrapProjects/{}/".format(id), name)
    os.makedirs(dir_name, exist_ok=True)


@proj.route("/delete", methods=['POST'])
@login_required
def delete():
    if request is not None:
        project_id = int(request.form['project_to_delete_id'])
        if 'delete' in request.form: 
            project = db.session.query(Project).filter_by(id=project_id).first()
            name = project.name
            #db.session.delete(project)
            #db.session.commit()
            project.deleted = True
            flash(_('%s has been permanently deleted.' % name))
        elif 'update' in request.form: 
            flash('Update pressed. Project id is {}'.format(project_id))
        elif 'download' in request.form: 
            flash('Download pressed.')
        
        #print(request.form['download'])
    return redirect(url_for('.dashboard'))

    