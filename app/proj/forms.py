from flask_wtf import Form
from wtforms import StringField, SubmitField, ValidationError, TextAreaField, HiddenField
from wtforms.validators import Required, Length, Regexp
from flask_babel import lazy_gettext
from ..models import Project


class NewProjectForm(Form):
    name = StringField(lazy_gettext('Name'), validators=[Required(), Length(1, 100)])
    url = StringField(lazy_gettext('URL'), validators=[Required(), Length(1, 1000)])
    requirements = TextAreaField(lazy_gettext('What to scrap'), validators=[Required()])
    submit = SubmitField(lazy_gettext('Get Quote'))
    
    
class ProjectButton(Form):
    download = SubmitField(lazy_gettext('Download'))
    update = SubmitField(lazy_gettext('Update'))
    delete = SubmitField(lazy_gettext('Delete'))